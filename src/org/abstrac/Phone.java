package org.abstrac;

public class Phone extends Telephone {

void with()
{
	System.out.println("with");
}
void lift()
{
	System.out.println("lift");
}
void disconnected()
{
	System.out.println("disconnected");
}

public static void main(String[] args) {
	
	Telephone obj = new Phone();
	obj.with();
	obj.lift();
	obj.disconnected();
	
}
	
}
