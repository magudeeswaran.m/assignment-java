package org.abstrac;

abstract class Telephone {

	abstract void with();
	abstract void lift();
	abstract void disconnected();
	
}
