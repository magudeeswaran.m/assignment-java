package com.sample;

import java.util.ArrayList;
import java.util.Iterator;

public class IteraterProgram {

	public static void main(String[] args) {
		
		ArrayList<String> arr = new ArrayList<>();
		
		arr.add("a");
		arr.add("b");
		arr.add("c");
		arr.add("d");
		arr.add("e");
		
		System.out.println(arr);
		
		Iterator<String> itr = arr.iterator();
		while(itr.hasNext())
		{
			Object fetch_value = itr.next();
			System.out.print(fetch_value+" ");
		}

	}

}
