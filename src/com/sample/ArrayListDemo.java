package com.sample;

import java.util.ArrayList;
import java.util.Iterator;

public class ArrayListDemo {

	public static void main(String[] args) {
		
		ArrayList<String> list = new ArrayList<>();
		System.out.println(list.size());
		
		list.add("a");
		list.add("b");
		list.add("c");
		list.add("d");
		list.add("e");
		
		System.out.println(list.size()); // Elements stored in consecutive memory location
		
		list.add("f");
		System.out.println(list);
		System.out.println(list.size());
		
		// replace the second element with B1
		list.set(1, "B1");
		
		System.out.println(list);
		
		list.remove(3);
		
		System.out.println(list);
		
		list.remove("e");
		
		System.out.println(list);
		
	}

}
