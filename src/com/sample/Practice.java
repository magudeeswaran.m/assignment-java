package com.sample;

public class Practice {

	int a=0; //instance variable
	
	static int b=5; // static variable
	
	private void sample() {
		
		int c=20; // local variable
		a=a+5;
		
		System.out.println(a);

	}
	public static void main(String[] args) {
		// Practice p = new Practice
		
		a=a+20; // we cannot call a instance variable 
		System.out.println(a);
		
		b=b-5;
		System.out.println(b);
		
		c=c*2; // the scope of local variable is only in the method
		
	}
	
	
}
