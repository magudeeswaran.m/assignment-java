package com.sample;

import java.util.LinkedList;

public class LinkedListProgram {

	public static void main(String[] args) {
		
		LinkedList<String> list = new LinkedList<>();
		
		list.add("a");
		list.add("b");
		list.add("c");
		list.add("d");
		list.add("e");
		
		System.out.println(list);
		list.add(1, "a1");
		System.out.println(list);
		list.addFirst("x");
		list.addLast("z");
		System.out.println(list);
		list.remove("d");
		System.out.println(list);

	}

}

// a ponits to b, b points to c, c points to d, d points to e

// add a1 after a

// a ponits to a1, a1 points to b, b points to c, c points to d, d poinds to e



