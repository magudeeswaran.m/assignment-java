package com.sample;


// An abstract class is a class it's consists of both abstract and non abstract method
// Non abstract method is not compulsary. It can have all the methods as abstract methods.
// An abstract method is a method without a body, It is just declared.(Contains the declaration)
// Abstract can't be instantiated, that is we can't create object for that class.
// AN child class can implement all the abstract methods present in the parent class (abstract class).
// What happens if a child does not implement those abstract method ?


public abstract class Abstraction {

	public abstract void employee(); //Abstract method
	
	public void department() // Non abstract method
	{
		
	}

}
