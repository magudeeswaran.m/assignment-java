package com.multilevel;

public class Child extends Parent {
	public void Property3()
	{
		System.out.println("child method");
	}
	
	public static void main(String[] args) {
		Grandparent gp=new Grandparent();
		Parent p=new Parent();
		Child c=new Child();
		gp.Property1();
//		gp.Property2();
//		gp.Property3();
		p.Property1();
		p.Property2();
//		p.Property3();
		c.Property1();
		c.Property2();
		c.Property3();
	}
	
}
