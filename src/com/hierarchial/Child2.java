package com.hierarchial;

public class Child2 extends Parent{
	public void property3()
	  {
		System.out.println("child2 class method");
	  }
	public static void main(String[] args) {
		Parent p=new Parent();
		Child1 c1=new Child1();
		Child2 c2=new Child2();
		p.property1();
//		p.property2();
//		p.property3();
		
		c1.property1();
//		c1.property2();
//		c1.property3();
		
		c2.property1();
//		c2.property2();
//		c2.property3();
	}
}
