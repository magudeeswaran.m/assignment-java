package com.stringoperations;

import java.util.Iterator;
import java.util.TreeSet;

public class Treeset {

	public static void main(String[] args) {
		
		TreeSet<String> set=new TreeSet<String>();  
		  set.add("Arun");  
		  set.add("Bala");  
		  set.add("Chandru");  
		  set.add("Deepak");  
		  //Traversing elements  
		  Iterator<String> itr=set.iterator();  
		  while(itr.hasNext()){  
		   System.out.println(itr.next());  
		  }  

		  System.out.println("Intial Set: "+set);  
          
	      System.out.println("Head Set: "+set.headSet("Chandru"));  
	          
	      System.out.println("SubSet: "+set.subSet("Arun", "Deepak"));  
	           
	      System.out.println("TailSet: "+set.tailSet("Chandru"));  
	}

}
