package com.stringoperations;

public class CheckUpper_Lower {

	public static void main(String[] args) {
		
		String str1="hello";
		String str2="HELLO";
		
		String str3=str1.toUpperCase();
		String str4=str1.toLowerCase();
		
		System.out.println(str3);
		System.out.println(str4);
	}

}
