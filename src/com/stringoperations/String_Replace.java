package com.stringoperations;

public class String_Replace {

	public static void main(String[] args) {
		
		String str1="hello";
		String str2="HELLO";
		
		String str3=str1.replace('e','E');
		String str4=str1.replace('E', 'e');
		
		System.out.println(str3);
		System.out.println(str4);

	}

}
