package com.stringoperations;

public class Palindrome {

	public static void main(String[] args) {
		
		String str="mam";
		
		String reverse="";
		
		int length=str.length();
		
		for(int i=length-1;i>=0;i--)
		{
			reverse=reverse + str.charAt(i);
		}
		
		if(str.equals(reverse))
		{
			System.out.println("Palindrome");
		}
		
		else
		{
			System.out.println("Not a Palindrome");
		}

	}

}
