package com.stringoperations;

import java.util.HashMap;
import java.util.Map;

public class Hashmap {

	public static void main(String[] args) {
		
		HashMap <Integer,String> map=new HashMap<Integer,String>();
	    map.put(1,"Ashok");
		map.put(2,"Vijay");//  key,value pair
		map.put(3,"Rahul");
		map.put(4,"prem");
		
		System.out.println(map);
		
		map.remove(3);
		
		 for(Map.Entry m : map.entrySet()){    
			    System.out.println(m.getKey()+" "+m.getValue());    
			   } 
		 
		 map.replace(2, "Vijay"); 
		 
		 for(Map.Entry m : map.entrySet()){    
			    System.out.println(m.getKey()+" "+m.getValue());    
			   } 

	}

}
